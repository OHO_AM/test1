import requests


url_v = 'https://gitlab.oho.wiki/api/v4/projects/528/variables/'
url_p = 'https://gitlab.oho.wiki/api/v4/projects/528/wikis/Test-project-page'

data_project={
	'access_token': 'pwsNQwTzhqretF2noFvE',
}

variables = {
	'subcat' : 'Harvesters',
	'licence' : 'CCBYNCSA40',
	'organization' : 'OHO',
	'project_status' : 'active',
	'maturity' : '',
	'design_files_pub' : 'yes',
	'cad_files_editable' : 'yes',
	'trans_status' : 'MT',
	'cad_format' : '',
	'mechanical_licence' : 'CCBYNCSA40',
	'electronic_files_pub' : '',
	'software_files_pub' : '',
	'assembly_inst_pub' : 'yes',
	'bill_materials_pub' : 'yes',
	'contains_pdf_drawings' : '',
	'technology_readyness_level' : '',
}


def create_vars():
	for key, value in variables.items():
		data = {
			'key': key,
			'value': value
		}
	response = requests.post(url_v, params=data_project, data=data)
	if response.status_code == 201:
		print(f'{key} variable created successfully')
	else:
		print(f'Error creating {key} variable: {response.content}')

def read_vars():
	res = ''
	for key, value in variables.items():
		data = {
			'key': key,
			'value': value
		}
		response = requests.get(url_v+key, params=data_project)
		valx = response.json()['value']
		if response.status_code == 200:
			res += f'- {key} : {valx}\n'
		else:
			print(f'Error creating {key} variable: {response.content}')
	return res

def update_page():
	data = {
	'content': read_vars()
	}
	response = requests.put(url_p, params=data_project, data=data)
	if response.status_code == 200:
		print('Wiki page updated successfully')
	else:
		print(f'Error updating Wiki page: {response.content}')

update_page()

# preq = requests.get(
#     	url = 'https://gitlab.oho.wiki/api/v4/projects/528/variables',
#     	params = data_project    	
# )
# idp = str(preq.json())
# print(idp)
